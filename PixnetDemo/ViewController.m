//
//  ViewController.m
//  PixnetDemo
//
//  Created by dennis on 2014/12/3.
//  Copyright (c) 2014年 dennis. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<FBLoginViewDelegate>

@property (weak, nonatomic) IBOutlet FBLoginView *loginView;
@property (weak, nonatomic) IBOutlet FBProfilePictureView *profilePictureView;
@property (weak, nonatomic) IBOutlet UILabel *nameLebel;
@property (weak, nonatomic) IBOutlet UIButton *postButton;
- (IBAction)clickPostButton:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Method

- (void)handleAuthError:(NSError *)error {
    NSString *alertText;
    NSString *alertTitle;
    if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
        // Error requires people using you app to make an action outside your app to recover
        alertTitle = NSLocalizedString(@"AlertTitle_AuthError", nil);
        alertText = [FBErrorUtility userMessageForError:error];
        [self showMessage:alertText withTitle:alertTitle];
        
    }else{
        // You need to find more information to handle the error within your app
        if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled){
            //The user refused to log in into your app, either ignore or...
            alertTitle = NSLocalizedString(@"AlertTitle_LoginCancelled", nil);
            alertText = NSLocalizedString(@"AlertMessage_LoginCancelled", nil);
            [self showMessage:alertText withTitle:alertTitle];
            
        }else{
            // All other errors that can happen need retries
            // Show the user a generic error message
            alertTitle = NSLocalizedString(@"AlertTitle_UnknowError", nil);
            alertText = NSLocalizedString(@"AlertMessage_UnknowError", nil);
            [self showMessage:alertText withTitle:alertTitle];
        }
    }
}

- (void)showMessage:(NSString *)text withTitle:(NSString *)title {
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:self
                      cancelButtonTitle:NSLocalizedString(@"AlertButton", nil)
                      otherButtonTitles:nil] show];
}

- (void)postMessageToFB:(NSString *)message withURL:(NSURL *)url andImage:(UIImage *)image {
    if ([FBDialogs canPresentOSIntegratedShareDialogWithSession:[FBSession activeSession]]){
        //check device supported
        [FBDialogs presentOSIntegratedShareDialogModallyFrom:self initialText:message image:image url:url handler:^(FBOSIntegratedShareDialogResult result, NSError *error) {

                switch (result) {
                    case FBOSIntegratedShareDialogResultSucceeded:
                        [self showMessage:NSLocalizedString(@"AlertMessage_PostSucceeded", nil)
                                withTitle:NSLocalizedString(@"AlertTitle_PostSucceeded", nil)];
                        break;
                    case FBOSIntegratedShareDialogResultCancelled:
                        [self showMessage:NSLocalizedString(@"AlertMessage_PostCanceled", nil)
                                withTitle:NSLocalizedString(@"AlertTitle_PostCanceled", nil)];
                        break;
                    case FBOSIntegratedShareDialogResultError:
                        [self handleAuthError:error];
                        break;
                    default:
                        break;
            }
        }];
    }

}


#pragma mark - FB Login View Delegate

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user {
    self.profilePictureView.profileID = user.objectID;
    self.nameLebel.text = user.name;
}

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    self.postButton.hidden = NO;
}

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    self.profilePictureView.profileID = nil;
    self.nameLebel.text = nil;
    self.postButton.hidden = YES;
}

- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    [self handleAuthError:error];
}


#pragma mark - Action

- (IBAction)clickPostButton:(id)sender {
    [self postMessageToFB:@"Hello world!" withURL:nil andImage:nil];
}
@end
