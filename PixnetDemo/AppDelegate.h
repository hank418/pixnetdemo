//
//  AppDelegate.h
//  PixnetDemo
//
//  Created by dennis on 2014/12/3.
//  Copyright (c) 2014年 dennis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

